<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * v3.2
 */

/**
 * OAuth2授权登录
 */

return array (
    'qq' => array (
        'key' => '100265581',
        'use' => 0,
        'name' => 'QQ',
        'icon' => 'qq',
        'secret' => 'ec7116e7767bafc94308d48cfe1b1dd3',
    ),
    'sina' => array (
        'key' => '3124513167',
        'use' => 0,
        'name' => '新浪微博',
        'icon' => 'sina',
        'secret' => 'ca0b65e3df315ba3e82374c496bbbaa0',
    ),
    'weixin' => array (
        'key' => '',
        'use' => 0,
        'name' => '微信',
        'icon' => 'weixin',
        'secret' => '',
    ),
);
<?php

/**
 * v3.1.0
 */

/**
 * 主表结构（由开发者定义）
 */


return array (
  'sql' => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL COMMENT \'栏目id\',
  `title` varchar(255) DEFAULT NULL COMMENT \'主题\',
  `thumb` varchar(255) DEFAULT NULL COMMENT \'缩略图\',
  `keywords` varchar(255) DEFAULT NULL COMMENT \'关键字\',
  `description` text COMMENT \'描述\',
  `hits` mediumint(8) unsigned DEFAULT NULL COMMENT \'浏览数\',
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者id\',
  `author` varchar(50) NOT NULL COMMENT \'作者名称\',
  `status` tinyint(2) NOT NULL COMMENT \'状态\',
  `url` varchar(255) DEFAULT NULL COMMENT \'地址\',
  `link_id` int(10) NOT NULL DEFAULT \'0\' COMMENT \'同步id\',
  `tableid` smallint(5) unsigned NOT NULL COMMENT \'附表id\',
  `inputip` varchar(15) DEFAULT NULL COMMENT \'录入者ip\',
  `inputtime` int(10) unsigned NOT NULL COMMENT \'录入时间\',
  `updatetime` int(10) unsigned NOT NULL COMMENT \'更新时间\',
  `comments` int(10) unsigned NOT NULL COMMENT \'评论数量\',
  `favorites` int(10) unsigned NOT NULL COMMENT \'收藏数量\',
  `displayorder` tinyint(3) NOT NULL DEFAULT \'0\',
  `csnf` int(10) unsigned DEFAULT NULL,
  `xingbie` varchar(255) DEFAULT NULL,
  `gznx` varchar(255) DEFAULT NULL,
  `zuigaoxueli` varchar(255) DEFAULT NULL,
  `lxdh` varchar(255) DEFAULT NULL,
  `gzdd` mediumint(8) unsigned DEFAULT NULL,
  `qiwangxinzi` varchar(255) DEFAULT NULL,
  `zhiweileibie` int(10) DEFAULT NULL,
  `goumai_total` int(10) unsigned DEFAULT \'0\' COMMENT \'表单统计\',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`,`updatetime`),
  KEY `link_id` (`link_id`),
  KEY `comments` (`comments`),
  KEY `favorites` (`favorites`),
  KEY `status` (`status`),
  KEY `hits` (`hits`),
  KEY `displayorder` (`displayorder`,`updatetime`),
  KEY `goumai_total` (`goumai_total`)
) ENGINE=MyISAM AUTO_INCREMENT=207 DEFAULT CHARSET=utf8 COMMENT=\'主表\'',
  'field' => 
  array (
    0 => 
    array (
      'fieldname' => 'csnf',
      'fieldtype' => 'Date',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'format' => 'Y-m-d',
          'value' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '出生年份',
    ),
    1 => 
    array (
      'fieldname' => 'xingbie',
      'fieldtype' => 'Radio',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'options' => '男|1
女|2',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '性别',
    ),
    2 => 
    array (
      'fieldname' => 'gznx',
      'fieldtype' => 'Text',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '工作年限',
    ),
    3 => 
    array (
      'fieldname' => 'zuigaoxueli',
      'fieldtype' => 'Select',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'options' => '没有|1
小学|2
中学|3
大学|4',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '最高学历',
    ),
    4 => 
    array (
      'fieldname' => 'lxdh',
      'fieldtype' => 'Text',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '联系电话',
    ),
    5 => 
    array (
      'fieldname' => 'gzdd',
      'fieldtype' => 'Linkage',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'linkage' => 'address',
          'value' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '工作地点',
    ),
    6 => 
    array (
      'fieldname' => 'zhiweileibie',
      'fieldtype' => 'Diy',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'type' => '1',
          'code' => '',
          'file' => 'hangye.php',
          'value' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '职位类别',
    ),
    7 => 
    array (
      'fieldname' => 'qiwangxinzi',
      'fieldtype' => 'Text',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '期望薪资',
    ),
    8 => 
    array (
      'fieldname' => 'title',
      'fieldtype' => 'Text',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => 'VARCHAR',
          'fieldlength' => '255',
        ),
        'validate' => 
        array (
          'required' => '1',
          'pattern' => '',
          'errortips' => '',
          'xss' => '1',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => 'onblur="check_title();get_keywords(\'keywords\');"',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '姓名',
    ),
    9 => 
    array (
      'fieldname' => 'thumb',
      'fieldtype' => 'File',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'size' => '10',
          'ext' => 'jpg,gif,png',
          'uploadpath' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '头像',
    ),
    10 => 
    array (
      'fieldname' => 'keywords',
      'fieldtype' => 'Text',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '1',
      'disabled' => '1',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => 400,
          'fieldtype' => 'VARCHAR',
          'fieldlength' => '255',
        ),
        'validate' => 
        array (
          'xss' => 1,
          'formattr' => ' data-role="tagsinput"',
        ),
      ),
      'displayorder' => '0',
      'textname' => '关键字',
    ),
    11 => 
    array (
      'fieldname' => 'description',
      'fieldtype' => 'Textarea',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '1',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '1',
      'disabled' => '1',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => 500,
          'height' => 60,
          'fieldtype' => 'VARCHAR',
          'fieldlength' => '255',
        ),
        'validate' => 
        array (
          'xss' => 1,
          'filter' => 'dr_clearhtml',
        ),
      ),
      'displayorder' => '0',
      'textname' => '描述',
    ),
  ),
);?>
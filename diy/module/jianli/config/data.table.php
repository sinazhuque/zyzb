<?php

/**
 * v3.1.0
 */

/**
 * 附表结构（由开发者定义）
 */


return array (
  'sql' => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL,
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者uid\',
  `catid` smallint(5) unsigned NOT NULL COMMENT \'栏目id\',
  `content` mediumtext COMMENT \'内容\',
  UNIQUE KEY `id` (`id`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT=\'附表\'',
  'field' => 
  array (
    0 => 
    array (
      'fieldname' => 'xingming2',
      'fieldtype' => 'Group',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'value' => '<label>{title}</label>  <label> {xingbie}</label>',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '-1',
      'textname' => '姓名',
    ),
    1 => 
    array (
      'fieldname' => 'qiuzhiyaoqiu',
      'fieldtype' => 'Merge',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'value' => '
{zhiweileibie}
{gzdd}
{qiwangxinzi}',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '求职要求',
    ),
    2 => 
    array (
      'fieldname' => 'jianlixinxi',
      'fieldtype' => 'Merge',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'value' => '
{content}',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '简历信息',
    ),
    3 => 
    array (
      'fieldname' => 'content',
      'fieldtype' => 'Ueditor',
      'relatedid' => '16',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '90%',
          'height' => '400',
          'autofloat' => '0',
          'autoheight' => '0',
          'autodown' => '0',
          'divtop' => '0',
          'page' => '0',
          'mode' => '1',
          'tool' => '\'bold\', \'italic\', \'underline\'',
          'mode2' => '1',
          'tool2' => '\'bold\', \'italic\', \'underline\'',
          'mode3' => '1',
          'tool3' => '\'bold\', \'italic\', \'underline\'',
          'value' => '',
        ),
        'validate' => 
        array (
          'required' => '1',
          'pattern' => '',
          'errortips' => '',
          'xss' => '1',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '详细介绍',
    ),
  ),
);?>
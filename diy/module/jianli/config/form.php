<?php

/**
 * v3.1.0
 */

/**
 * 表单的结构（此文件由导出产生，无需开发者定义）
 */


return array (
  3 => 
  array (
    'sql' => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL COMMENT \'内容id\',
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者id\',
  `author` varchar(50) NOT NULL COMMENT \'作者名称\',
  `inputip` varchar(30) DEFAULT NULL COMMENT \'录入者ip\',
  `inputtime` int(10) unsigned NOT NULL COMMENT \'录入时间\',
  `title` varchar(255) DEFAULT NULL COMMENT \'内容主题\',
  `url` varchar(255) DEFAULT NULL COMMENT \'内容地址\',
  `subject` varchar(255) DEFAULT NULL COMMENT \'表单主题\',
  `tableid` smallint(5) unsigned NOT NULL COMMENT \'附表id\',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`),
  KEY `author` (`author`),
  KEY `inputtime` (`inputtime`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT=\'购买表单数据表\'',
    'sql2' => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL COMMENT \'内容id\',
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者id\',
  UNIQUE KEY `id` (`id`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT=\'购买表单附表\'',
    'data' => 
    array (
      'id' => '3',
      'module' => 'jianli',
      'name' => '购买',
      'table' => 'goumai',
      'disabled' => '0',
      'permission' => '{"0":{"disabled":"1","notedit":"1","experience":"","score":"","postnum":"","postcount":""},"1":{"experience":"","score":"","postnum":"","postcount":""},"2":{"experience":"","score":"","postnum":"","postcount":""},"3_1":{"experience":"","score":"","postnum":"","postcount":""},"3_2":{"experience":"","score":"","postnum":"","postcount":""},"3_3":{"experience":"","score":"","postnum":"","postcount":""},"3_4":{"experience":"","score":"","postnum":"","postcount":""},"4_5":{"experience":"","score":"","postnum":"","postcount":""},"4_6":{"experience":"","score":"","postnum":"","postcount":""},"4_7":{"experience":"","score":"","postnum":"","postcount":""},"4_8":{"experience":"","score":"","postnum":"","postcount":""},"5_9":{"experience":"","score":"","postnum":"","postcount":""}}',
      'setting' => '{"icon":"fa fa-rmb","rt_url":""}',
    ),
    'field' => 
    array (
      0 => 
      array (
        'id' => '255',
        'name' => '主题',
        'fieldname' => 'subject',
        'fieldtype' => 'Text',
        'relatedid' => '3',
        'relatedname' => 'mform-jianli',
        'isedit' => '1',
        'ismain' => '1',
        'issystem' => '1',
        'ismember' => '1',
        'issearch' => '1',
        'disabled' => '0',
        'setting' => '{"option":{"width":300,"fieldtype":"VARCHAR","fieldlength":"255"},"validate":{"xss":1,"required":1}}',
        'displayorder' => '0',
      ),
    ),
  ),
);?>
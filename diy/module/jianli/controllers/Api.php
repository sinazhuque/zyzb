<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
	

 
class Api extends M_Controller {

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }
	
	/**
     * 购买文档
     */
	public function buy() {
		$this->_show_buy();
	}
	
	/**
     * 收藏文档
     */
	public function favorite() {
		$this->api_favorite();
	}

	/**
     * 购买简历
     */
	public function goumai() {

        if (!$this->uid) {
            // 未登录
            exit($this->callback_json(array(
                'msg' => '没有登录',
                'code' => 0
            )));
        }

        $price = $this->get_cache('module-'.SITE_ID.'-'.APP_DIR, 'setting', 'jianli');
        if (!$price) {
            // 未登录
            exit($this->callback_json(array(
                'msg' => '管理员没有设置简历单价，无法进行购买',
                'code' => 0
            )));
        }

        $id = $cid = (int)$this->input->get('id');
        $data = $this->db->where('id', $id)->select('url,title')->get(SITE_ID.'_'.APP_DIR)->row_array();
        if (!$data) {
            // 文档不存在
            exit($this->callback_json(array(
                'msg' => '简历不存在',
                'code' => 0
            )));
        }

        $table = SITE_ID.'_'.APP_DIR.'_form_goumai';
        $goumai = $this->db->where('cid', $id)->where('uid', $this->uid)->get($table)->row_array();
        if ($goumai) {
            exit($this->callback_json(array(
                'msg' => '你已经购买过了',
                'code' => 0
            )));
        } elseif ($this->member['money'] - $price < 0) {
            exit($this->callback_json(array(
                'msg' => '余额不足，本次购买需要'.$price.'元',
                'code' => 0
            )));
        } else {
            // 添加成功
            $this->db->insert($table, array(
                'cid' => $id,
                'uid' => $this->uid,
                'author' => $this->member['username'],
                'inputip' => '',
                'subject' => '购买金额：'.$price.'元',
                'url' => '',
                'title' => '',
                'inputtime' => SYS_TIME,
            ));
            $id = $this->db->insert_id();
            if ($id) {
                $this->load->model('pay_model');
                $this->pay_model->add($this->uid, -$price, '<a href="'.$data['url'].'" target="_blank">购买简历《'.$data['title'].'》</a>');
                // 更新数量
                $c = $this->db->where('cid', $cid)->count_all_results($table);
                $this->db->where('id', $cid)->set('goumai_total', $c)->update(SITE_ID.'_'.APP_DIR);
                exit($this->callback_json(array(
                    'msg' => '购买成功',
                    'code' => 1
                )));
            } else {
                exit($this->callback_json(array(
                    'msg' => '系统错误',
                    'code' => 0
                )));
            }
        }
	}
	
	/**
     * 站点间的同步登录
     */
	public function synlogin() {
		$this->api_synlogin();
	}
	
	/**
     * 站点间的同步退出
     */
	public function synlogout() {
		$this->api_synlogout();
	}
	
	/**
     * 自定义信息JS调用
     */
	public function template() {
		$this->api_template();
	}
	
}
<?php

/**
 * v3.1.0
 */

/**
 * 附表结构（由开发者定义）
 */


return array (
  'sql' => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL,
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者uid\',
  `catid` smallint(5) unsigned NOT NULL COMMENT \'栏目id\',
  `content` mediumtext COMMENT \'内容\',
  `lianxiren` varchar(255) DEFAULT NULL,
  `lxdh` varchar(255) DEFAULT NULL,
  `xxjd` varchar(255) DEFAULT NULL,
  `ditubiaozhu_lng` decimal(9,6) DEFAULT NULL,
  `ditubiaozhu_lat` decimal(9,6) DEFAULT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT=\'附表\'',
  'field' => 
  array (
    0 => 
    array (
      'fieldname' => 'lianxiren',
      'fieldtype' => 'Text',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '联系人',
    ),
    1 => 
    array (
      'fieldname' => 'lxdh',
      'fieldtype' => 'Text',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '200',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '联系电话',
    ),
    2 => 
    array (
      'fieldname' => 'xxjd',
      'fieldtype' => 'Text',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '400',
          'is_mb_auto' => '0',
          'value' => '',
          'fieldtype' => '',
          'fieldlength' => '',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '详细街道',
    ),
    3 => 
    array (
      'fieldname' => 'ditubiaozhu',
      'fieldtype' => 'Baidumap',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '700',
          'height' => '430',
          'level' => '15',
          'city' => '北京',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '0',
      'textname' => '地图标注',
    ),
    4 => 
    array (
      'fieldname' => 'danweixinxi',
      'fieldtype' => 'Merge',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '0',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'value' => '
{lianxiren}
{lxdh}
{gongzuodiqu}
{xxjd}
{ditubiaozhu}',
        ),
        'validate' => 
        array (
          'required' => '0',
          'pattern' => '',
          'errortips' => '',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '10',
      'textname' => '单位信息',
    ),
    5 => 
    array (
      'fieldname' => 'content',
      'fieldtype' => 'Ueditor',
      'relatedid' => '15',
      'relatedname' => 'module',
      'isedit' => '1',
      'ismain' => '0',
      'issystem' => 1,
      'ismember' => '1',
      'issearch' => '0',
      'disabled' => '0',
      'setting' => 
      array (
        'option' => 
        array (
          'width' => '100%',
          'height' => '400',
          'autofloat' => '0',
          'autoheight' => '0',
          'autodown' => '0',
          'divtop' => '0',
          'page' => '0',
          'mode' => '1',
          'tool' => '\'bold\', \'italic\', \'underline\'',
          'mode2' => '1',
          'tool2' => '\'bold\', \'italic\', \'underline\'',
          'mode3' => '1',
          'tool3' => '\'bold\', \'italic\', \'underline\'',
          'value' => '',
        ),
        'validate' => 
        array (
          'required' => '1',
          'pattern' => '',
          'errortips' => '',
          'xss' => '1',
          'check' => '',
          'filter' => '',
          'tips' => '',
          'formattr' => '',
        ),
        'is_right' => '0',
      ),
      'displayorder' => '9',
      'textname' => '任职要求',
    ),
  ),
);?>
<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
	

 
class Api extends M_Controller {

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }
	
	/**
     * 购买文档
     */
	public function buy() {
		$this->_show_buy();
	}
	
	/**
     * 收藏文档
     */
	public function favorite() {
		$this->api_favorite();
	}

	/**
     * 投递简历选择简历
     */
	public function toudi() {

        if (!$this->uid) {
            // 未登录
            exit('没有登录');
        }

        $data = $this->db->where('uid', $this->uid)->select('id,title')->get(SITE_ID.'_jianli')->result_array();
        if (!$data) {
            exit('你还没有创建过简历');
        }

        $this->template->assign('list', $data);
        $this->template->display('toudi.html');
    }

	/**
     * 投递简历
     */
	public function toudi_jianli() {

        if (!$this->uid) {
            // 未登录
            exit($this->callback_json(array(
                'msg' => '没有登录',
                'code' => 0
            )));
        }

        $cid = (int)$this->input->get('id');
        if (!$cid) {
            exit($this->callback_json(array(
                'msg' => '招聘id不存在',
                'code' => 0
            )));
        }

        $title = (int)$this->input->post('title');
        if (!$title) {
            exit($this->callback_json(array(
                'msg' => '未选择简历投递',
                'code' => 0
            )));
        }

        $data = $this->db->where('uid', $this->uid)->where('id', $title)->get(SITE_ID.'_jianli')->row_array();
        if (!$data) {
            exit($this->callback_json(array(
                'msg' => '简历不存在',
                'code' => 0
            )));
        }


        $table = SITE_ID.'_'.APP_DIR.'_form_jianli';
        $goumai = $this->db->where('cid', $cid)->where('title', $title)->where('uid', $this->uid)->get($table)->row_array();
        if ($goumai) {
            exit($this->callback_json(array(
                'msg' => '你已经投递过了',
                'code' => 0
            )));
        } else {
            // 添加成功
            $this->db->insert($table, array(
                'cid' => $cid,
                'uid' => $this->uid,
                'author' => $this->member['username'],
                'inputip' => '',
                'subject' => '投递简历：'.$data['title'].'',
                'url' => $data['url'],
                'title' => $title,
                'inputtime' => SYS_TIME,
            ));
            $id = $this->db->insert_id();
            if ($id) {
                // 更新数量
                $c = $this->db->where('cid', $cid)->count_all_results($table);
                $this->db->where('id', $cid)->set('jianli_total', $c)->update(SITE_ID.'_'.APP_DIR);
                exit($this->callback_json(array(
                    'msg' => '投递成功',
                    'code' => 1
                )));
            } else {
                exit($this->callback_json(array(
                    'msg' => '系统错误',
                    'code' => 0
                )));
            }
        }
    }
	
	/**
     * 站点间的同步登录
     */
	public function synlogin() {
		$this->api_synlogin();
	}
	
	/**
     * 站点间的同步退出
     */
	public function synlogout() {
		$this->api_synlogout();
	}
	
	/**
     * 自定义信息JS调用
     */
	public function template() {
		$this->api_template();
	}
	
}
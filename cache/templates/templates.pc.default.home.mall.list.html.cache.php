<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>list<?php echo $meta_title; ?></title>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>" />
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/style/css/pagination.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/style/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/style/css/diy.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/js/layer/skin/layer.css" />
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery.flexslider-min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/js/cms.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/diy.js"></script>
    <script type="text/javascript" src="<?php echo THEME_PATH; ?>/js/dayrui.js"></script>
    <script type="text/javascript" src="<?php echo THEME_PATH; ?>/js/layer/layer.js"></script>

    <style type="text/css">
        .screen-box dl dd a:hover{
            color: #DC2523;
        }
    </style>

    <script type="text/javascript">
        $(function(){
            //TAB切换
            tabs('#goodsTabs','click');
            //智能浮动层
            $("#tabHead").smartFloat();
            //初始化规格事件
            //initGoodsSpec('{config.webpath}tools/submit_ajax.ashx?action=get_article_goods_info&channel_id={model.channel_id}');
            check_btn_isok();

            //check_checkbox_change();

            //文字收藏
            shoucang_select_wenzi();

            //文字购物车
            gouwuche_select_wenzi();
        });



        function dr_tips(msg, time, code) {

            if (!time) {
                time = 3000;
            }
            var tip = '<i class="fa fa-times-circle"></i>';
            //var theme = 'teal';
            if (code == 1) {
                tip = '<i class="fa fa-check-circle"></i>';
                //theme = 'lime';
            } else if (code >= 2) {
                tip = '<i class="fa fa-info-circle"></i>';
                //theme = 'ruby';
            }
            //alert(tip+'&nbsp;&nbsp;'+msg.stripHTML());return;
            //layer.msg(tip+'&nbsp;&nbsp;'+msg.stripHTML());
            alert(msg.stripHTML());

        }

    </script>

    <script type="text/javascript">
        $(function(){
            //TAB切换
            tabs('#goodsTabs','click');
            //智能浮动层
            $("#tabHead").smartFloat();
            //初始化规格事件
            //initGoodsSpec('{config.webpath}tools/submit_ajax.ashx?action=get_article_goods_info&channel_id={model.channel_id}');
            check_btn_isok();

        });

        function dr_tips(msg, time, code) {

            if (!time) {
                time = 3000;
            }
            var tip = '<i class="fa fa-times-circle"></i>';
            //var theme = 'teal';
            if (code == 1) {
                tip = '<i class="fa fa-check-circle"></i>';
                //theme = 'lime';
            } else if (code >= 2) {
                tip = '<i class="fa fa-info-circle"></i>';
                //theme = 'ruby';
            }
            //alert(tip+'&nbsp;&nbsp;'+msg.stripHTML());return;
            //layer.msg(tip+'&nbsp;&nbsp;'+msg.stripHTML());
            alert(msg.stripHTML());

        }

    </script>

    <script language="javascript">


        function check_btn_isok(){
            var kucun = $("#commodityStockNum").text();
            //var kucun = $("#commoditySelectNum").val();
            //console.log(kucun);
            //$('.buy over')
            //库存大于0
            if(kucun >0){
                $(".buy").attr("class","buy");
                $(".add").attr("class","add");
            }


        }


        $(window).ready(
            function () {
                var show =  $("#showArea");

                var left = 0;

                var picBox = $(".smallPic");

                var bigPic = $("#main_img");

                var img = $("#showArea img");

                var speed = img.width()*4;

                show.css("width",((img.length+1)*img.width()).toString()+"px");
                img.hover(
                    function () {
                        bigPic.attr("src",this.src);
                        $(".MagicZoomBigImageCont img").attr("src",this.src);
                        $(".MagicThumb-container img").attr("href",this.src);
                        $("#zoom1").attr("href",this.src);
                    }
                );



                $("#gobottom").click(
                    function () {
                        if (parseInt(show.css("left"))-speed <= (-(show.width() - picBox.width()+speed))) {
                            return 0;
                        }
                        show.animate({
                            "left" : left -= speed
                        },200)
                    }
                );
                $("#gotop").click(
                    function () {
                        if (show.css("left") >= "0px") {
                            return 0;
                        }
                        show.animate({
                            "left": left += speed
                        }, 200)

                    }
                );
                var sku_price
            }

        );

        <?php $sku_sn = $sku_price = $sku_quantity = array();  if (is_array($order_specification['value'])) { $count=count($order_specification['value']);foreach ($order_specification['value'] as $iid=>$t) {  $sku_price[$iid] = $t['price'];  $sku_sn[$iid] = $t['sn'];  $sku_quantity[$iid] = $t['quantity'];  } } ?>
            var sku_sn = <?php echo json_encode($sku_sn); ?>;
            var sku_price = <?php echo json_encode($sku_price); ?>;
            var sku_quantity = <?php echo json_encode($sku_quantity); ?>;

            // 商品数量加减选择
            function dr_sku_item_num(op,id) {
                //input 获取购买数量框
                var num = parseInt($("#commoditySelectNum_"+id).val());

                //库存 dr_sku_quantity commodityStockNum
                var _quantity = parseInt($("#commodityStockNum_"+id).attr("value"));

                if (isNaN(_quantity) || _quantity <= 0) {
                    dr_tips("库存不足无法购买", 3);
                    return;
                }

                if (isNaN(num) || num < 1) num = 0;
                //console.log(num);
                if (op) {
                    // 加操作
                    num++;
                    if (num >= _quantity) num = _quantity;
                } else {
                    // 减操作
                    num--;
                    //if (isNaN(num) || num <= 1) num = 1;
                    if (isNaN(num) || num < 1) num = 0;
                }
                $("#commoditySelectNum_"+id).val(num);

            }

            function update_scart() {
                $.get("/index.php?s=order&c=cart&m=nums", function(data){
                    var vall = "购物车("+data.code+')';
                    $('#scart').text(vall) ;
                }, 'jsonp');
            }

            // 购买商品
            function dr_buy() {
                <?php if (!$member) { ?>
                dr_login();
                return;
                <?php } ?>
                    var order = "<?php echo dr_url('order/home/index', array('mid'=>MOD_DIR, 'cid'=>$id)); ?>";
                    <?php if ($order_specification) { ?>
                    var i = 0;
                    var sku = new Array();
                    $(".dr_sku_value").each(function(){
                        var id = $(this).val();
                        if (id == "") {
                            // 请勾选您要的商品信息
                            dr_tips("请勾选您要的商品信息", 3);
                            return;
                        }
                        sku[i] = id;
                        i++;
                    });
                    order+= "&spec="+sku.join("_"); // 组合串联属性
                    <?php } ?>
                        // 判断库存  // commodityStockNum  dr_sku_quantity
                        var _quantity = parseInt($("#commodityStockNum").html());
                        if (isNaN(_quantity) || _quantity <= 0) {
                            dr_tips("库存不足无法购买", 3);
                            return;
                        }
                        var num = parseInt($("#dr_sku_num").val());
                        order+= "&num="+num;
                        location.href=order;
                    }
                    // 购物车
                    var is_cart = true;
                    function dr_cart() {
                        if (!is_cart) {
                            //alert('刷新后再试');
                            //return;
                        }
                        var cart = "<?php echo dr_url('order/cart/add', array('mid'=>MOD_DIR, 'cid'=>$id)); ?>";
                        <?php if ($order_specification) { ?>
                        var i = 0;
                        var sku = new Array();
                        $(".dr_sku_value").each(function(){
                            var id = $(this).val();
                            //alert(id);return;
                            if (id == "") {
                                // 请勾选您要的商品信息
                                dr_tips("请勾选您要的商品信息", 3);
                                return;
                            }
                            sku[i] = id;
                            i++;
                        });
                        cart+= "&spec="+sku.join("_");
                        <?php } ?>
                            // 判断库存   commodityStockNum   dr_sku_quantity
                            var _quantity = parseInt($("#commodityStockNum").html());
                            if (isNaN(_quantity) || _quantity <= 0) {
                                dr_tips("库存不足无法购买", 3);
                                return;
                            }
                            //dr_sku_num
                            var num = parseInt($("#commoditySelectNum").val());
                            cart+= "&num="+num;
                            is_cart = false;
                            $.ajax({type: "GET", url: cart, dataType:"jsonp",
                                success: function (data) {
                                    if (data.status) {
                                        dr_tips('添加成功，购车中共'+data.code+'件商品', 3, 1);
                                        update_scart();
                                    } else {
                                        dr_tips(data.code);
                                        is_cart = true;
                                    }
                                }
                            });
                        }
                        // 按规则更新商品价格
                        function dr_sku_update_item() {
                            var i = 0;
                            var sku = new Array();
                            // 遍历属性
                            $(".dr_sku_value").each(function(){
                                var id = $(this).val();
                                if (id == "") return;
                                sku[i] = id;
                                i++;
                            });
                            var oname = sku.join("_"); // 组合串联属性
                            // 当全部勾选之后才更新商品价格与库存
                            var _sn = sku_sn[oname];
                            var _price = sku_price[oname];
                            var _quantity = sku_quantity[oname];
                            $("#dr_sku_price").html(price_float(_price));
                            $("#dr_sku_sn").html(_sn);
                            if (_quantity) {
                                $("#dr_sku_quantity").html(_quantity);
                            } else {
                                $("#dr_sku_quantity").html("<font color=red>库存不足</font>");
                            }
                        }
                        // 按规则选择商品
                        function dr_sku_select_item(_this, oname, val) {
                            $(".dr_sku_"+oname+" > span").attr("class", "txt");
                            $(_this).attr("class", "curr");
                            $("#dr_sku_"+oname).val(val);
                            dr_sku_update_item();
                        }
                        function price_float(value){  //保留两位小数点
                            if (!value) value = 0;
                            value = Math.round(parseFloat(value) * 100) / 100;
                            if (value.toString().indexOf(".") < 0) {
                                value = value.toString() + ".00";
                            }
                            return value;
                        }
                        // 设置商品的默认规则
                        $(function(){
                            $(".dr_sku_value").each(function(){
                                var id = $(this).val();
                                var oname = $(this).attr("oname");
                                $(".dr_sku_"+oname+" > span").attr("class", "txt");
                                if (id) {
                                    $("#dr_sku_value_"+id).attr("class", "curr");
                                } else {
                                    var _first = $(".dr_sku_"+oname+" span").first();
                                    var _iid = _first.attr("ovalue");
                                    _first.attr("class", "curr");
                                    $("#dr_sku_"+oname).val(_iid);
                                }
                                dr_sku_update_item();
                            });
                        });



    </script>


</head>

<body id="goods">
<!--页面头部-->
<?php if ($fn_include = $this->_include("header.html", "/")) include($fn_include); ?>
<!--/页面头部-->

<!--当前位置-->
<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<!--/当前位置-->

<!--/分类导航-->
<div class="section">
    <div class="wrapper clearfix">
        <div class="screen-box">
            <!--分类-->
            <dl>
                <dt>产品大类：</dt>
                <dd>
                    <!--调用栏目下面的商品属性筛选-->
                    <a class="selected" href="<?php echo dr_search_url($params, 'chanpindalei', ''); ?>">全部</a>

                    <?php $zujin=array('素硬金'=>'素硬金','CNC硬金'=>'CNC硬金','宝石硬金'=>'宝石硬金','珐琅硬金'=>'珐琅硬金','珍珠硬金'=>'珍珠硬金');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                    <a class="<?php if ($params['chanpindalei'] == $i) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'chanpindalei', $t); ?>"><?php echo $t; ?></a>
                    <?php } } ?>

                </dd>

            </dl>
            <!--/分类-->

            <!--款式类别-->
            <dl>
                <dt>款式类别：</dt>
                <dd>
                    <!--调用栏目下面的商品属性筛选-->
                    <a class="selected" href="<?php echo dr_search_url($params, 'kslb', ''); ?>">全部</a>
                    <?php $zujin=array('传统文化'=>'传统文化','时尚潮流'=>'时尚潮流','可爱系列'=>'可爱系列','宝宝系列'=>'宝宝系列','十二生肖'=>'十二生肖','专利产品'=>'专利产品','套装系列'=>'套装系列','十二星座'=>'十二星座');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                    <a class="<?php if ($params['kslb'] == $i) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'kslb', $t); ?>"><?php echo $t; ?></a>
                    <?php } } ?>
                </dd>

            </dl>
            <!--款式类别-->

            <!--款式形状-->
            <dl>
                <dt>款式形状：</dt>
                <dd>
                    <!--调用栏目下面的商品属性筛选-->
                    <a class="selected" href="<?php echo dr_search_url($params, 'ksxz', ''); ?>">全部</a>
                    <?php $zujin=array('星座'=>'星座','其他'=>'其他','动物'=>'动物','如意'=>'如意','鼠'=>'鼠','牛'=>'牛','虎'=>'虎','兔'=>'兔','龙'=>'龙','蛇'=>'蛇','马'=>'马','羊'=>'羊','猴'=>'猴','鸡'=>'鸡','狗'=>'狗','猪'=>'猪','猫'=>'猫','凤'=>'凤','鸟'=>'鸟','熊'=>'熊','鱼'=>'鱼','花'=>'花','佛'=>'佛','蜜蜂'=>'蜜蜂','貔貅'=>'貔貅','麒麟'=>'麒麟','福袋'=>'福袋','葫芦'=>'葫芦','福禄寿'=>'福禄寿','十字架'=>'十字架','蝴蝶'=>'蝴蝶','人物'=>'人物','水果'=>'水果','蘑菇'=>'蘑菇','足印'=>'足印','叶子'=>'叶子','狐狸'=>'狐狸','心形'=>'心形','钥匙'=>'钥匙','天鹅'=>'天鹅','观音'=>'观音','斧头'=>'斧头','生肖'=>'生肖');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                    <a class="<?php if ($params['ksxz'] == $i) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'ksxz', $t); ?>"><?php echo $t; ?></a>
                    <?php } } ?>
                </dd>

            </dl>
            <!--款式形状-->

            <!--款式品名-->
            <dl>
                <dt>款式品名：</dt>
                <dd>
                    <!--调用栏目下面的商品属性筛选-->
                    <a class="selected" href="<?php echo dr_search_url($params, 'kspm', ''); ?>">全部</a>
                    <?php $zujin=array('吊坠'=>'吊坠','项链'=>'项链','戒指'=>'戒指','手镯/手链'=>'手镯/手链','摆件'=>'摆件','胸针'=>'胸针','脚链'=>'脚链','耳环/耳钉'=>'耳环/耳钉','路路通'=>'路路通');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                    <a class="<?php if ($params['kspm'] == $i) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'kspm', $t); ?>"><?php echo $t; ?></a>
                    <?php } } ?>
                </dd>

            </dl>
            <!--款式品名-->

            <!--测试组-->
            <!--           <dl>
                            <dt>测试组：</dt>
                            <dd>
                               &lt;!&ndash;调用栏目下面的商品属性筛选&ndash;&gt;
                                <a class="selected" href="<?php echo dr_search_url($params, 'kspm', ''); ?>">全部</a>
                                <?php $zujin=array('吊坠'=>'吊坠','项链'=>'项链','戒指'=>'戒指','手镯/手链'=>'手镯/手链','摆件'=>'摆件','胸针'=>'胸针','脚链'=>'脚链','耳环/耳钉'=>'耳环/耳钉','路路通'=>'路路通');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                                <a class="<?php if ($params['kspm'] == $i) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'kspm', $t); ?>"><?php echo $t; ?></a>
                                <?php } } ?>
                            </dd>

                        </dl>-->
            <!--测试组-->


            <!--价格-->
            <dl>
                <dt>金重：</dt>
                <dd>
                    <a class="selected" href="<?php echo dr_search_url($params, 'order_price', ''); ?>">全部</a>
                    <!--自定义一个租金返回数组，这个都懂得，php最基础的数组用法-->
                    <?php $zujin=array('0,1'=>'1克以内','1,2'=>'1到2克','2,3'=>'2到3克','3,4'=>'3到4克','4,0'=>'4克以上');  if (is_array($zujin)) { $count=count($zujin);foreach ($zujin as $i=>$t) { ?>
                    <a class="<?php if (strstr($params['order_price'],$i)) { ?> selected<?php } ?>" href="<?php echo dr_search_url($params, 'order_price', $i); ?>"><?php echo $t; ?></a>
                    <?php } } ?>
                </dd>
            </dl>
            <!--/价格-->
        </div>

        <div class="sort-box">

            <p style="line-height:30px;float: left; width: 50%">
                <strong>排序方式：</strong>
                <label>
                    <select class="form-control" onchange="dr_search_order(this.value)">
                        <option <?php if (!$params['order']) { ?>selected<?php } ?> value="">默认排序</option>
                        <option <?php if ($params['order'] == 'order_volume_desc') { ?>selected<?php } ?> value="order_volume_desc">销量由高到低</option>
                        <option <?php if ($params['order'] == 'order_volume_asc') { ?>selected<?php } ?> value="order_volume_asc">销量由低到高</option>
                        <option <?php if ($params['order'] == 'order_price_desc') { ?>selected<?php } ?> value="order_price_desc">重量由多到少</option>
                        <option <?php if ($params['order'] == 'order_price_asc') { ?>selected<?php } ?> value="order_price_asc">重量由少到多</option>
                    </select>
                </label>
            </p>
            <p class="search-paixu">
                <!--搜索-->
            <div style="float:right; margin-right: 10px;">
                <div class="input-group">
                    <input type="text" class="search-box-diy" onkeypress="if(event.keyCode==13) {searchByClass();return false;}" name='keyword' value='<?php echo $keyword; ?>' id='dr_search_keyword'>
                    <span class="input-group-btn">
                                            <button class="search-btn-diy" onclick="searchByClass()" type="button">产品搜索</button>
                    </span>
                </div>
                <script type="text/javascript">
                    function searchByClass(){
                        var url="<?php echo dr_search_url($params, 'keyword', 'dayruicom'); ?>";
                        var value=$("#dr_search_keyword").val();
                        if (value) {
                            location.href=url.replace('dayruicom', value);
                        } else {
                            dr_tips("输入关键字");
                        }
                    }

                    function dr_search_order(value) {
                        var url="<?php echo dr_search_url($params, 'order', 'dayruicom'); ?>";
                        location.href=url.replace('dayruicom', value);
                    }
                </script>
            </div>
            <!--搜索-->
            </p>

        </div>




    </div>
</div>
<!--/分类导航-->

<!--商品列表-->
<div class="section">
    <div class="wrapper clearfix">
        <ul class="img-list">
            <!--取得数据-->

            <!--分页页码-->
            <?php $return = array();$list_temp = $this->list_tag("action=module catid=$catid order=updatetime page=1 pagesize=20"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
            <li>

                <a href="<?php echo $t['url']; ?>">
                    <p class="z-search-p"><?php echo dr_strcut($t['title'], 10); ?></p>
                    <div class="img-box">

                        <div class="abs-txt">推荐</div>
                        <img src="<?php echo dr_thumb($t['thumb']['0'], 220, 220); ?>" width="220" height="220" style="margin-top: 15px;">
                    </div>
                    <div class="info">
                        <p class="price">金重:<b><?php echo $t['order_price']; ?></b>克<strong value="<?php echo $t['order_quantity']; ?>"><input id="commodityStockNum_<?php echo $t['id']; ?>" type="hidden" value="<?php echo $t['order_quantity']; ?>" />库存<?php echo $t['order_quantity']; ?></strong></p>
                    </div>

                </a>
                <div class="info">
                    <dl>
                        <dt>购买数量：</dt>
                        <dd>
                            <div class="stock-box">
                                <input id="ajax_id_<?php echo $t['id']; ?>" type="hidden" value="<?php echo $t['id']; ?>" />
                                <input id="commodityChannelId" type="hidden" value="<?php echo $t['id']; ?>" />
                                <input id="commodityArticleId" type="hidden" value="<?php echo $t['id']; ?>" />
                                <input id="commodityGoodsId" type="hidden" value="0" />
                                <input id="commoditySelectNum_<?php echo $t['id']; ?>" type="text" maxlength="9" value="0" maxValue="" onkeydown="" />
                                <div style="width:17px; float: left">
                                    <a class="add" href="javascript:void(0);" onclick="dr_sku_item_num(1,<?php echo $t['id']; ?>);">+</a>
                                    <a class="remove" href="javascript:void(0);" onclick="dr_sku_item_num(0,<?php echo $t['id']; ?>);">-</a>
                                </div>
                            </div>

                        </dd>
                    </dl>
                </div>
                <div class="cart_select">
                    <!--<div class="gouwuche_select"><input name="gouwuche_select" type="checkbox" value="<?php echo $t['id']; ?>"  /> 加入购物车</div>-->
                    <!--<div class="shoucang_select"><input name="shoucang_select" type="checkbox" value="<?php echo $t['id']; ?>" /> 收藏</div>-->
                    <div class="gouwuche_select_wenzi" value="<?php echo $t['id']; ?>">加入购物车</div>
                    <div class="shoucang_select_wenzi" value="<?php echo $t['id']; ?>">收藏</div>
                </div>
            </li>
            <?php } } ?>
            <!--<div class="nodata">暂时无法找到您想要的商品！</div>-->

        </ul>

        <!--页码列表 display: flex; align-items: center;-->
        <div class="page-box" style="margin:15px 0 0;">
            <!--<div class="digg_page_styll">-->
                <div class="digg"><?php echo $pages; ?></div>
            <!--</div>-->
        </div>
        <!--/页码列表-->
    </div>

</div>
<!--/商品列表-->

<!--页面底部-->
<?php if ($fn_include = $this->_include("footer.html", "/")) include($fn_include); ?>
<!--/页面底部-->
</body>
</html>
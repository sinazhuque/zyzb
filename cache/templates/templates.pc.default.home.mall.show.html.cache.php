﻿<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $meta_title; ?></title>
    <meta name="keywords" content="{site.seo_keyword}" />
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <link type="text/css" href="<?php echo THEME_PATH; ?>/style/css/style.css" rel="stylesheet"/>
    <link type="text/css"  href="<?php echo THEME_PATH; ?>/style/css/diy.css" rel="stylesheet"/>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery.jqzoom.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/common.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/picture.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/cart.js"></script>
    <script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/js/cms.js"></script>
    <style type="text/css">
        .goods-tab .tab-head ul li a:hover, .goods-tab .tab-head ul li a.selected {
            color: #fff;
            background: #DC2523;
        }
    </style>
    <script type="text/javascript">
        $(function(){
            //TAB切换
            tabs('#goodsTabs','click');
            //智能浮动层
            $("#tabHead").smartFloat();
            //初始化规格事件
            //initGoodsSpec('{config.webpath}tools/submit_ajax.ashx?action=get_article_goods_info&channel_id={model.channel_id}');
            check_btn_isok();

            //模拟点击开启商品介绍
            $('#spjs').trigger("click");
        });

        function dr_tips(msg, time, code) {

            if (!time) {
                time = 3000;
            }
            var tip = '<i class="fa fa-times-circle"></i>';
            //var theme = 'teal';
            if (code == 1) {
                tip = '<i class="fa fa-check-circle"></i>';
                //theme = 'lime';
            } else if (code >= 2) {
                tip = '<i class="fa fa-info-circle"></i>';
                //theme = 'ruby';
            }
            //alert(tip+'&nbsp;&nbsp;'+msg.stripHTML());return;
            //layer.msg(tip+'&nbsp;&nbsp;'+msg.stripHTML());
            alert(msg.stripHTML());

        }

    </script>

    <script language="javascript">
        // 这是加入收藏夹的ajax请求，我就随意写了一下提示信息，至于美化啊什么交给你们了
        function dr_favorite() {
            $.get("/index.php?s=<?php echo MOD_DIR; ?>&c=api&m=favorite&id=<?php echo $id; ?>", function(data){
                if (data == 1) {
                    dr_tips("没有登录，不能收藏");
                } else if (data == 2) {销售排行
                    dr_tips("商品不存在，无法收藏");
                } else if (data == 3) {
                    dr_tips("更新收藏成功", 3, 1);
                } else if (data == 4) {
                    dr_tips("收藏成功", 3, 1);
                }
            });
        }

        //判断按钮是否ok
        function check_btn_isok(){
                var kucun = $("#commodityStockNum").text();
                //var kucun = $("#commoditySelectNum").val();
                //console.log(kucun);
                //$('.buy over')
                //库存大于0
                if(kucun >0){
                    $(".buy").attr("class","buy");
                    $(".add").attr("class","add");
                }


        }


        $(window).ready(
            function () {
                var show =  $("#showArea");

                var left = 0;

                var picBox = $(".smallPic");

                var bigPic = $("#main_img");

                var img = $("#showArea img");

                var speed = img.width()*4;

                show.css("width",((img.length+1)*img.width()).toString()+"px");
                img.hover(
                    function () {
                        bigPic.attr("src",this.src);
                        $(".MagicZoomBigImageCont img").attr("src",this.src);
                        $(".MagicThumb-container img").attr("href",this.src);
                        $("#zoom1").attr("href",this.src);
                    }
                );



                $("#gobottom").click(
                    function () {
                        if (parseInt(show.css("left"))-speed <= (-(show.width() - picBox.width()+speed))) {
                            return 0;
                        }
                        show.animate({
                            "left" : left -= speed
                        },200)
                    }
                );
                $("#gotop").click(
                    function () {
                        if (show.css("left") >= "0px") {
                            return 0;
                        }
                        show.animate({
                            "left": left += speed
                        }, 200)

                    }
                );
                var sku_price
            }

        );

        <?php $sku_sn = $sku_price = $sku_quantity = array();  if (is_array($order_specification['value'])) { $count=count($order_specification['value']);foreach ($order_specification['value'] as $iid=>$t) {  $sku_price[$iid] = $t['price'];  $sku_sn[$iid] = $t['sn'];  $sku_quantity[$iid] = $t['quantity'];  } } ?>
            var sku_sn = <?php echo json_encode($sku_sn); ?>;
            var sku_price = <?php echo json_encode($sku_price); ?>;
            var sku_quantity = <?php echo json_encode($sku_quantity); ?>;

            // 商品数量加减选择
            function dr_sku_item_num(op) {
                //console.log(op);return;
                //dr_sku_num  commoditySelectNum
                var num = parseInt($("#commoditySelectNum").val());

                //库存 dr_sku_quantity commodityStockNum
                var _quantity = parseInt($("#commodityStockNum").html());
                //console.log(_quantity);return;
                if (isNaN(num) || num <= 1) num = 1;
                if (op) {
                    // 加操作
                    num++;
                    if (num >= _quantity) num = _quantity;
                } else {
                    // 减操作
                    num--;
                    if (isNaN(num) || num <= 1) num = 1;
                }
                $("#commoditySelectNum").val(num);
            }

            function update_scart() {
                $.get("/index.php?s=order&c=cart&m=nums", function(data){
                    var vall = "购物车("+data.code+')';
                    $('#scart').text(vall) ;
                }, 'jsonp');
            }

            // 购买商品
            function dr_buy() {
                <?php if (!$member) { ?>
                dr_login();
                return;
                <?php } ?>
                    var order = "<?php echo dr_url('order/home/index', array('mid'=>MOD_DIR, 'cid'=>$id)); ?>";
                    <?php if ($order_specification) { ?>
                    var i = 0;
                    var sku = new Array();
                    $(".dr_sku_value").each(function(){
                        var id = $(this).val();
                        if (id == "") {
                            // 请勾选您要的商品信息
                            dr_tips("请勾选您要的商品信息", 3);
                            return;
                        }
                        sku[i] = id;
                        i++;
                    });
                    order+= "&spec="+sku.join("_"); // 组合串联属性
                    <?php } ?>
                        // 判断库存  // commodityStockNum  dr_sku_quantity
                        var _quantity = parseInt($("#commodityStockNum").html());
                        if (isNaN(_quantity) || _quantity <= 0) {
                            dr_tips("库存不足无法购买", 3);
                            return;
                        }
                        var num = parseInt($("#dr_sku_num").val());
                        order+= "&num="+num;
                        location.href=order;
                    }
                    // 购物车
                    var is_cart = true;
                    function dr_cart() {
                        if (!is_cart) {
                            //alert('刷新后再试');
                            //return;
                        }
                        var cart = "<?php echo dr_url('order/cart/add', array('mid'=>MOD_DIR, 'cid'=>$id)); ?>";
                        <?php if ($order_specification) { ?>
                        var i = 0;
                        var sku = new Array();
                        $(".dr_sku_value").each(function(){
                            var id = $(this).val();
                            //alert(id);return;
                            if (id == "") {
                                // 请勾选您要的商品信息
                                dr_tips("请勾选您要的商品信息", 3);
                                return;
                            }
                            sku[i] = id;
                            i++;
                        });
                        cart+= "&spec="+sku.join("_");
                        <?php } ?>
                            // 判断库存   commodityStockNum   dr_sku_quantity
                            var _quantity = parseInt($("#commodityStockNum").html());
                            if (isNaN(_quantity) || _quantity <= 0) {
                                dr_tips("库存不足无法购买", 3);
                                return;
                            }
                            //dr_sku_num
                            var num = parseInt($("#commoditySelectNum").val());
                            cart+= "&num="+num;
                            is_cart = false;
                            $.ajax({type: "GET", url: cart, dataType:"jsonp",
                                success: function (data) {
                                    if (data.status) {
                                        dr_tips('添加成功，购车中共'+data.code+'件商品', 3, 1);
                                        update_scart();
                                    } else {
                                        dr_tips(data.code);
                                        is_cart = true;
                                    }
                                }
                            });
                        }
                        // 按规则更新商品价格
                        function dr_sku_update_item() {
                            var i = 0;
                            var sku = new Array();
                            // 遍历属性
                            $(".dr_sku_value").each(function(){
                                var id = $(this).val();
                                if (id == "") return;
                                sku[i] = id;
                                i++;
                            });
                            var oname = sku.join("_"); // 组合串联属性
                            // 当全部勾选之后才更新商品价格与库存
                            var _sn = sku_sn[oname];
                            var _price = sku_price[oname];
                            var _quantity = sku_quantity[oname];
                            $("#dr_sku_price").html(price_float(_price));
                            $("#dr_sku_sn").html(_sn);
                            if (_quantity) {
                                $("#dr_sku_quantity").html(_quantity);
                            } else {
                                $("#dr_sku_quantity").html("<font color=red>库存不足</font>");
                            }
                        }
                        // 按规则选择商品
                        function dr_sku_select_item(_this, oname, val) {
                            $(".dr_sku_"+oname+" > span").attr("class", "txt");
                            $(_this).attr("class", "curr");
                            $("#dr_sku_"+oname).val(val);
                            dr_sku_update_item();
                        }
                        function price_float(value){  //保留两位小数点
                            if (!value) value = 0;
                            value = Math.round(parseFloat(value) * 100) / 100;
                            if (value.toString().indexOf(".") < 0) {
                                value = value.toString() + ".00";
                            }
                            return value;
                        }
                        // 设置商品的默认规则
                        $(function(){
                            $(".dr_sku_value").each(function(){
                                var id = $(this).val();
                                var oname = $(this).attr("oname");
                                $(".dr_sku_"+oname+" > span").attr("class", "txt");
                                if (id) {
                                    $("#dr_sku_value_"+id).attr("class", "curr");
                                } else {
                                    var _first = $(".dr_sku_"+oname+" span").first();
                                    var _iid = _first.attr("ovalue");
                                    _first.attr("class", "curr");
                                    $("#dr_sku_"+oname).val(_iid);
                                }
                                dr_sku_update_item();
                            });
                        });



    </script>

</head>

<body id="goods">
<!--页面头部-->
<?php if ($fn_include = $this->_include("header.html", "/")) include($fn_include); ?>
<!--/页面头部-->

<!--当前位置-->
<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<!--/当前位置-->

<div class="section">
    <div class="wrapper clearfix">
        <div class="wrap-box">
            <!--页面左边-->
            <div class="left-925">
                <div class="goods-box clearfix">
                    <!--商品图片-->
                    <div class="goods-pic">
                        <!--幻灯片-->
                        <div class="pic-box">
                            <div class="pic-preview">
                                <span class="jqzoom">
                                    <img />
                                </span>
                            </div>
                            <!--缩略图-->
                            <?php if (IS_PC) { ?>
                            <div class="pic-scroll">
                                <a class="prev">&lt;</a>
                                <a class="next">&gt;</a>
                                <div class="items">
                                    <ul>
                                        <?php if (is_array($thumb)) { $count=count($thumb);foreach ($thumb as $pic) { ?>
                                        <li><img bimg="<?php echo dr_get_file($pic); ?>" src="<?php echo dr_get_file($pic); ?>" onmousemove="preview(this);" /></li>
                                        <?php } } ?>
                                    </ul>
                                </div>
                            </div>
                            <?php } else {  } ?>
                            <!--缩略图-->
                        </div>
                        <!--/幻灯片-->
                    </div>
                    <!--/商品图片-->

                    <!--商品信息-->
                    <div class="goods-spec">
                        <!--<script type="text/javascript" src="{config.webpath}tools/submit_ajax.ashx?action=view_article_click&channel_id={model.channel_id}&id={model.id}&click=1&hide=1"></script>-->
                        <h1 name="mall_start"><?php echo $title; ?></h1>
                        <p class="subtitle"><?php echo dr_strcut($description, 60); ?></p>
                        <div class="spec-box">
                            <dl>
                                <dt>货号</dt>
                                <dd id="commodityGoodsNo"><?php echo $order_sn; ?></dd>
                            </dl>
                            <dl>
                                <dt>款式品名</dt>
                                <dd id="commodityGoodsNo"><?php echo $kspm; ?></dd>
                            </dl>
                            <dl>
                                <dt>产品大类</dt>
                                <dd id="commodityGoodsNo"><?php echo $chanpindalei; ?></dd>
                            </dl>
                            <dl>
                                <dt>款式类别</dt>
                                <dd id="commodityGoodsNo"><?php echo $kslb; ?></dd>
                            </dl>
                            <dl>
                                <dt>款式形状</dt>
                                <dd id="commodityGoodsNo"><?php echo $ksxz; ?></dd>
                            </dl>
                            <dl>
                                <dt>上架时间</dt>
                                <dd><?php echo $updatetime; ?></dd>
                            </dl>
                            <dl>
                                <dt>累计销量</dt>
                                <dd><?php echo $order_volume; ?></dd>
                            </dl>
                            <dl>
                                <dt>重量</dt>
                                <dd><em class="price" id="commoditySellPrice"><?php echo $order_price; ?></b>克</em></dd>
                            </dl>
                        </div>

                        <!--<div class="spec-box line" id="goodsSpecBox">
                            &lt;!&ndash;商品规格&ndash;&gt;
                            &lt;!&ndash;循环&ndash;&gt;
                            <dl>
                                <dt>{modelt1.title}</dt>
                                <dd>
                                    <ul class="items">
                                        &lt;!&ndash;规格选项&ndash;&gt;
                                        &lt;!&ndash;循环&ndash;&gt;
                                        <li>
                                            <a specid="{modelt2.spec_id}" title="{modelt2.title}" href="javascript:;">

                                                <img src="" />

                                                <span>规格图</span>

                                            </a>
                                        </li>
                                        &lt;!&ndash;循环&ndash;&gt;
                                        &lt;!&ndash;/规格选项&ndash;&gt;
                                    </ul>
                                </dd>
                            </dl>
                            &lt;!&ndash;循环&ndash;&gt;
                            &lt;!&ndash;/商品规格&ndash;&gt;
                        </div>-->

                        <div class="spec-box">
                            <dl>
                                <dt>购买数量</dt>
                                <dd>
                                    <div class="stock-box">
                                        <input id="commodityChannelId" type="hidden" value="{model.channel_id}" />
                                        <input id="commodityArticleId" type="hidden"  type="hidcommoditySelectNumden" value="{model.id}" />
                                        <input id="commodityGoodsId" type="hidden" value="0" />
                                        <!--<input id="commoditySelectNum" type="text" maxlength="9" value="1" maxValue="{model.fields[stock_quantity]}" onkeydown="return checkNumber(event);" />-->
                                        <input id="commoditySelectNum" type="text" maxlength="9" value="1" maxValue="" onkeydown="" />
                                        <!--<a class="add" onclick="addCartNum(1);">+</a>
                                        <a class="remove" onclick="addCartNum(-1);">-</a>-->
                                        <a class="add" href="javascript:void(0);" onclick="dr_sku_item_num(1);">+</a>
                                        <a class="remove" href="javascript:void(0);" onclick="dr_sku_item_num(0);">-</a>

                                    </div>
                                    <span class="stock-txt">
                                        库存<em id="commodityStockNum" value="<?php echo $order_quantity; ?>" ><?php echo $order_quantity; ?></em>件
                                    </span>
                                </dd>
                            </dl>
                            <dl>
                                <dd>
                                    <div class="btn-buy" id="buyButton">
                                        <button class="buy over" onclick="dr_buy();" disabled="disabled"><a href="javascript:;" onclick="dr_buy()">立即购买</a></button>
                                        <button class="add over" onclick="dr_cart();" disabled="disabled"><a href="javascript:;" onclick="dr_cart()">加入购物车</a></button>
                                        <!--<button><a class="btn-add-cart addcar" ii="<?php echo dr_get_file($thumb[0]); ?>" href="javascript:;" onclick="dr_cart()">加入购物车</a></button>  buy over-->
                                        <!--<button class="buy over" onclick="dr_buy();" disabled="disabled">立即购买</button>
                                        <button class="add over" onclick="dr_cart();" disabled="disabled">加入购物车</button>-->

                                    </div>
                                </dd>
                            </dl>
                        </div>
                        <div class="spec-box">
                            <dl>
                                <dd>
                                    <!--分享-->
                                    <!--<%template src="_share_js.html"%>-->
                                    <!--/分享-->
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <!--/商品信息-->
                </div>

                <div id="goodsTabs" class="goods-tab bg-wrap">
                    <!--选项卡-->
                    <div id="tabHead" class="tab-head">
                        <ul>
                            <li>
                                <a id="spjs" class="selected" href="javascript:;">商品介绍</a>
                            </li>
                            <!--<li>
                                <a id="sppl" href="javascript:;">商品评论</a>
                            </li>-->
                        </ul>
                    </div>
                    <!--/选项卡-->

                    <!--选项内容-->
                    <!--<div class="tab-content entry" style="display:block;">-->
                    <div class="tab-content entry" style="display:none;">
                        <?php echo $content; ?>
                    </div>

                    <!--<div class="tab-content">
                        网友评论

                        <div class="comment-box">
                           <?php echo dr_module_comment(MOD_DIR, $id); ?>
                        </div>
                        网友评论
                    </div>-->

                </div>

            </div>
            <!--/页面左边-->

            <!--页面右边-->
            <div class="left-220">
                <div class="bg-wrap nobg">
                    <div class="sidebar-box">
                        <h4>最新上架</h4>
                        <ul class="txt-list">
                            <?php $return = array();$list_temp = $this->list_tag("action=module module=mall catid=11 order=updatetime num=10"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>

                            <li>

                                <label class="hot"><?php echo $t['id']; ?></label>

                                <!--<label><?php echo $t['id']; ?></label>-->

                                <a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 28); ?></a>
                            </li>

                            <?php } } ?>
                        </ul>
                    </div>

                    <div class="sidebar-box">
                        <h4>推荐商品</h4>
                        <ul class="side-img-list">
                            <?php $return = array();$list_temp = $this->list_tag("action=module module=mall catid=11 order=updatetime num=10"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
                            <li>
                                <div class="img-box">
                                    <a href="<?php echo $t['url']; ?>">
                                    <img src="<?php echo dr_thumb($t['thumb']['0'], 60, 60); ?>" />
                                    </a>
                                </div>
                                <div class="txt-box">
                                    <a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 28); ?></a>
                                    <span><?php echo $t['updatetime']; ?></span>
                                </div>
                            </li>
                            <?php } } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!--/页面右边-->
        </div>
    </div>
</div>

<!--页面底部-->
<?php if ($fn_include = $this->_include("footer.html", "/")) include($fn_include); ?>
<!--/页面底部-->

<script type="text/javascript">
    window.location = "#mall_start"; //自动跳转到锚点处
</script>

</body>
</html>
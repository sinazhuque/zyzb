<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $meta_title; ?></title>
	<meta name="keywords" content="<?php echo $meta_keywords; ?>" />
	<meta name="description" content="<?php echo $meta_description; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/style/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_PATH; ?>/style/css/diy.css" />
	<script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jquery.flexslider-min.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/jqslider.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/style/js/common.js"></script>
	<script type="text/javascript" charset="utf-8" src="<?php echo THEME_PATH; ?>/js/cms.js"></script>



	<script type="text/javascript">
        $(function(){
            $("#slide-box").jqslider(); //初始化幻灯片
            $("#focus-box").flexslider({
                directionNav: false,
                pauseOnAction: false
            });
        });


        //layer 提示
 /*       function dr_tips(msg, time, code) {
            if (!time) {
                time = 3000;
            }
            var tip = '<i class="fa fa-times-circle"></i>';
            //var theme = 'teal';
            if (code == 1) {
                tip = '<i class="fa fa-check-circle"></i>';
                //theme = 'lime';
            } else if (code >= 2) {
                tip = '<i class="fa fa-info-circle"></i>';
                //theme = 'ruby';
            }
            //alert(tip+'&nbsp;&nbsp;'+msg.stripHTML());
            //layer.msg(tip+'&nbsp;&nbsp;'+msg.stripHTML());
            alert(msg.stripHTML());

        }
*/

	</script>
</head>

<body id="index">
<!--页面头部-->
<?php if ($fn_include = $this->_include("header.html")) include($fn_include); ?>
<!--/页面头部-->

<!--Banner-->
<div id="slide-box" class="slide-box">
	<ul class="list-box">
		<?php $return = array();$list_temp = $this->list_tag("action=navigator type=1"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
			<li><a target="_blank"><img src="<?php echo dr_thumb($t['thumb']); ?>" /></a></li>
		<?php } } ?>
	</ul>
</div>
<!--/Banner-->

<!--新闻资讯-->
<div class="section">
	<div class="main-tit">
		<h2>新闻资讯</h2>
		<p>
			<!--
			<%set DataTable newsCList=get_category_child_list("news",0)%>
			<%foreach(DataRow dr in newsCList.Rows)%>
			<a href="<%linkurl("news_list",{dr[id]})%>">{dr[title]}</a>
			<%/foreach%>
			<a href="<%linkurl("news")%>">更多展示<i>+</i></a>
			-->
		</p>
	</div>
	<div class="wrapper clearfix">
		<div class="wrap-box">
			<div class="left-455" style="margin:0;height:341px;">
				<div id="focus-box" class="focus-box">
					<ul class="slides">

<!--循环-->
						<?php $return = array();$list_temp = $this->list_tag("action=navigator type=5"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
						<li>
							<a title="<?php echo $t['title']; ?>" href="<?php echo $t['url']; ?>">
							<span class="note-bg"></span>
							<span class="note-txt">图片展示</span>
							<img src="<?php echo dr_thumb($t['thumb']); ?>" />
							</a>
						</li>
						<?php } } ?>


					</ul>
				</div>
			</div>
			<div class="left-455">
				<ul class="side-txt-list">
					<!--调用新闻模块的“首页中间”属性的最新10条-->
					<!--<li class="tit"><a href="<%linkurl("news_show",{newdr[id]})%>">{newdr[title]}</a></li>-->
					<?php $return = array();$list_temp = $this->list_tag("action=module module=news order=updatetime num=0,1"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
					<li class="tit"><a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 20); ?></a></li>
					<?php } }  $return = array();$list_temp = $this->list_tag("action=module module=news order=updatetime num=1,4"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
					<!--<li style="line-height: 23px"><span class="badge badge-empty badge-success"></span>&nbsp;<a href="<?php echo $t['url']; ?>" class="title"><?php echo dr_strcut($t['title'], 28); ?></a></li>-->
					<li><span><?php echo $t['updatetime']; ?></span><a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 23); ?></a></li>
					<!--<li style="line-height: 23px"><span class="badge badge-empty badge-success"></span>&nbsp;<a href="<?php echo $t['url']; ?>" class="title"><?php echo dr_strcut($t['title'], 28); ?></a></li>-->
					<?php } }  $return = array();$list_temp = $this->list_tag("action=module module=news order=updatetime num=5,1"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
					<li class="tit"><a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 20); ?></a></li>
					<?php } }  $return = array();$list_temp = $this->list_tag("action=module module=news order=updatetime num=6,4"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
					<!--<li style="line-height: 23px"><span class="badge badge-empty badge-success"></span>&nbsp;<a href="<?php echo $t['url']; ?>" class="title"><?php echo dr_strcut($t['title'], 28); ?></a></li>-->
					<li><span><?php echo $t['updatetime']; ?></span><a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 23); ?></a></li>
					<!--<li style="line-height: 23px"><span class="badge badge-empty badge-success"></span>&nbsp;<a href="<?php echo $t['url']; ?>" class="title"><?php echo dr_strcut($t['title'], 28); ?></a></li>-->
					<?php } } ?>


				</ul>
			</div>
			<div class="left-220">
				<ul class="side-img-list">
					<?php $return = array();$list_temp = $this->list_tag("action=module thumb=1 module=news catid=10 order=updatetime num=4"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
					<li>
						<div class="img-box">
							<label><?php echo dr_strcut($t['title'], 1); ?></label>
							<!--<img src="<?php echo dr_thumb(60,60,100,0); ?>" />-->
							<img src="<?php echo dr_thumb($t['thumb'], 60, 60); ?>" />
						</div>
						<div class="txt-box">
							<a href="<?php echo $t['url']; ?>"><?php echo dr_strcut($t['title'], 16); ?></a>
							<span><?php echo $t['updatetime']; ?></span>
						</div>
					</li>
					<?php } } ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--/新闻资讯-->

<!--/购物商城-->
<div class="section">
	<div class="main-tit">
		<h2>购物商城</h2>
		<p>
			<?php $return = array();$list_temp = $this->list_tag("action=category module=share pid=11"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
			<a href="<?php echo $t['url']; ?>" title="<?php echo $t['name']; ?>"><?php echo $t['name']; ?></a>
			<?php } } ?>

			<a href="/index.php?c=category&id=11">更多<i>+</i></a>
		</p>
	</div>
	<div class="wrapper clearfix">
		<div class="wrap-box">
			<ul class="img-list">
				<?php $return = array();$list_temp = $this->list_tag("action=module module=mall order=updatetime num=0,10"); if ($list_temp) extract($list_temp); $count=count($return); if (is_array($return)) { foreach ($return as $key=>$t) { $is_first=$key==0 ? 1 : 0;$is_last=$count==$key+1 ? 1 : 0; ?>
				<li>
					<a title="<?php echo dr_strcut($t['title'], 28); ?>" href="<?php echo $t['url']; ?>">
					<p class="z-search-p"><?php echo dr_strcut($t['title'], 10); ?></p>
					<div class="img-box">
						<img src="<?php echo dr_thumb($t['thumb']['0'], 220, 220); ?>" width="220" height="220" style="margin-top: 15px;">
					</div>
						<div class="info">
							<p class="price">金重:<b><?php echo $t['order_price']; ?></b>克<strong>库存 <?php echo $t['order_quantity']; ?></strong></p>
						</div>
					</a>
				</li>
				<?php } } ?>
			</ul>
		</div>
	</div>
</div>
<!--/购物商城-->


<!--页面底部-->
<?php if ($fn_include = $this->_include("footer.html")) include($fn_include); ?>
<!--/页面底部-->
</body>
</html>
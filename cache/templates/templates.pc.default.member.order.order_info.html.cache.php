<?php if ($fn_include = $this->_include("order_header.html")) include($fn_include); ?>

<div class="row">
    <div class="col-md-12">
        <div class="portlet  portlet-fit ">
            <div class="portlet-body">

                <div class="portlet-title">
                    <div class="caption dr_info">
                        <p>订单号：<a href="javascript:;" class="btn green btn-xs"><?php echo $order['sn']; ?></a></p>
                        <?php echo dr_order_member_option($member['uid'], $order); ?>
                    </div>

                </div>

                <div class="row" style="clear: both">
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet yellow-crusta box">
                            <div class="portlet-title">
                                <div class="caption"><i class="fa fa-shopping-cart"></i> 订单信息 </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">
                                    <div class="col-md-5 name">订单状态：</div>
                                    <div class="col-md-7 value"> <?php echo dr_order_status($order); ?></div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 下单时间：</div>
                                    <div class="col-md-7 value"> <?php echo dr_date($order['order_time']); ?> </div>
                                </div>
                                <?php if ($member['uid'] != $order['buy_uid']) { ?>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 买家账号：</div>
                                    <div class="col-md-7 value"> <?php echo $order['buy_username']; ?>  </div>
                                </div>
                                <?php }  if ($member['uid'] != $order['sell_uid']) { ?>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 商家账号：</div>
                                    <div class="col-md-7 value"> <?php echo $order['sell_username']; ?> </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet blue-hoki box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-rmb"></i> 支付信息 </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 订单重量： </div>
                                    <div class="col-md-7 value"> <?php if ($order['order_score']) {  echo intval($order['order_price']);  echo SITE_SCORE;  } else { ?>克<?php echo number_format($order['order_price'], 2); ?>元 <?php } ?></div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 付款时间： </div>
                                    <div class="col-md-7 value"> <?php if ($order['pay_time']) {  echo dr_date($order['pay_time']);  } else { ?>未付款<?php } ?> </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 支付方式： </div>
                                    <div class="col-md-7 value"> <?php if ($paytype[$order['pay_type']]) {  echo $paytype[$order['pay_type']]['name'];  } else { ?>未知<?php } ?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet green-meadow box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-home"></i> 收货人信息
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">
                                    <div class="col-md-12 value">
                                        <label><?php echo $order['shipping_name']; ?></label>
                                        <label><?php echo $order['shipping_phone']; ?></label>
                                        <label><?php echo $order['shipping_zipcode']; ?></label>
                                        <label><?php echo $order['shipping_city'];  echo $order['shipping_address']; ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet red-sunglo box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-bus"></i> 运输信息
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 发货时间： </div>
                                    <div class="col-md-7 value"> <?php echo dr_date($order['shipping_time']); ?> </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 承运者： </div>
                                    <div class="col-md-7 value"> <?php if ($kd[$order['shipping_type']]) {  echo $kd[$order['shipping_type']];  } else { ?>无<?php } ?> </div>
                                </div>
                                <?php if ($order['shipping_sn']) { ?>
                                <div class="row static-info">
                                    <div class="col-md-5 name"> 运输单号： </div>
                                    <div class="col-md-7 value"> <?php echo $order['shipping_sn']; ?> </div>
                                </div>
                                <div class="row static-info">
                                    <div class="col-md-12 value" id="kd_info"> </div>
                                    <script>
                                        $(function(){
                                            $.get("<?php echo dr_member_url('order/home/kd'); ?>&name=<?php echo $order['shipping_type']; ?>&sn=<?php echo $order['shipping_sn']; ?>", function(data){
                                                $('#kd_info').html(data);
                                            });
                                        });
                                    </script>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet yellow-haze box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-user"></i> 买家留言
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if ($order['buy_note']) {  echo $order['buy_note'];  } else { ?>
                                无
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="portlet blue-soft box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-file-o"></i> 其他信息
                                </div>
                            </div>
                            <div class="portlet-body">
                                <?php if ($field) {  if ($fn_include = $this->_include("myfield.html")) include($fn_include);  } else { ?>
                                无
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet grey-cascade box">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-shopping-cart"></i> 商品清单
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="table-responsive">
                                    <table class="table table-hover table-bordered table-striped">
                                        <thead>
                                        <tr>
                                            <th class="hidden-480"> 商品Id </th>
                                            <th> 商品名称 </th>
                                            <th> 商品重量 </th>
                                            <th> 购买数量 </th>
                                            <th>  </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php if (is_array($order['goods'])) { $count=count($order['goods']);foreach ($order['goods'] as $t) { ?>
                                        <tr>
                                            <td class="hidden-480">
                                                <a target="_blank" href="<?php echo $t['url']; ?>"><?php echo $t['cid']; ?></a>
                                            </td>
                                            <td>
                                                <a class="" target="_blank" href="<?php echo $t['url']; ?>"><?php echo $t['title'];  $sku=dr_string2array($t['sku']); if ($sku) { ?>
                                                    （<?php if (is_array($sku)) { $count=count($sku);foreach ($sku as $s=>$u) { ?><span><?php echo $s; ?>：<?php echo $u; ?> </span><?php } } ?> ）
                                                    <?php }  if ($t['sn']) { ?><p>商品编号：<?php echo $t['sn']; ?></p><?php } ?>
                                                </a>
                                            </td>
                                            <td> <?php if ($order['order_score']) {  echo intval($t['price']);  echo SITE_SCORE;  } else {  echo number_format($t['price'], 2); ?>克<?php } ?></td>
                                            <td> <?php echo $t['quantity']; ?> </td>
                                            <td>
                                                <a href="<?php echo SITE_URL; ?>index.php?s=<?php echo MOD_DIR; ?>&mid=<?php echo $t['mid']; ?>&spec=<?php echo $t['specification']; ?>&cid=<?php echo $t['cid']; ?>&num=<?php echo $t['quantity']; ?>" target="_blank" class="btn blue btn-xs"> <i class="fa fa-shopping-cart"></i> 再次购买</a>
                                                <?php if ($comment[$t['id']]) { ?>
                                                <a href="<?php echo SITE_URL; ?>index.php?s=<?php echo $t['mid']; ?>&c=comment&m=index&id=<?php echo $t['cid']; ?>&oid=<?php echo $order['id']; ?>&gid=<?php echo $t['id']; ?>" target="_blank" class="btn green btn-xs"> <i class="fa fa-comments"></i>  商品评论</a> </td>
                                                <?php } ?>
                                        </tr>
                                        <?php } } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6"> </div>
                    <div class="col-md-6">
                        <div class="well">
                            <div class="row static-info align-reverse">
                                <div class="col-md-7 name"> 优惠了： </div>
                                <div class="col-md-5 value"> 克<?php echo number_format($order['order_youhui'], 2); ?> 元</div>
                            </div>
                            <div class="row static-info align-reverse">
                                <div class="col-md-7 name"> 运费： </div>
                                <div class="col-md-5 value"> 克<?php echo number_format($order['shipping_price'], 2); ?> 元</div>
                            </div>
                            <div class="row static-info align-reverse">
                                <div class="col-md-7 name"> 应付总额： </div>
                                <div class="col-md-5 value"> <?php if ($order['order_score']) {  echo intval($order['order_price']);  echo SITE_SCORE;  } else { ?>克<?php echo number_format($order['order_price'], 2); ?> 元<?php } ?></div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>

<?php if ($fn_include = $this->_include("nfooter.html")) include($fn_include); ?>
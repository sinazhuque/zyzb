/*
*作者：jennings
*时间：2018-11-13
*自写独立模块若干js
*规则 必须独立标注每个模块的模块信息，模板位置，以及程序作用
----------------------------------------------------------*/


/*
* 模块：mall 商城
* 模板：mall/search.html
* 程序作用：商城首页/搜索页  收藏功能
* */

//tips消息提示
function dr_tips(msg, time, code) {

    if (!time) {
        time = 3000;
    }
    var tip = '<i class="fa fa-times-circle"></i>';
    //var theme = 'teal';
    if (code == 1) {
        tip = '<i class="fa fa-check-circle"></i>';
        //theme = 'lime';
    } else if (code >= 2) {
        tip = '<i class="fa fa-info-circle"></i>';
        //theme = 'ruby';
    }
    //alert(tip+'&nbsp;&nbsp;'+msg.stripHTML());return;
    layer.msg(tip+'&nbsp;&nbsp;'+msg.stripHTML());
    //alert(msg.stripHTML());
    //var timer = 3000;
    //setTimeout("ifrtemp.location.reload();",timer);

}


// 这是加入收藏夹的ajax请求
function dr_favorite(id) {
    //console.log(id);return;
    $.get("/index.php?s=mall&c=api&m=favorite&id="+id, function(data){
        //console.log(data);return;
        if (data == 1) {
            dr_tips("没有登录，不能收藏",3,1);
        } else if (data == 2) {
            dr_tips("商品不存在，无法收藏",3,1);
        } else if (data == 3) {
            dr_tips("更新收藏成功", 3, 1);
        } else if (data == 4) {
            //dr_tips("收藏成功", 3, 1);
            console.log('收藏成功');
        }
    });
}

// 这是取消加入收藏夹的ajax请求
function dr_favorite_del(id) {
    //console.log(id);return;
    $.get("/index.php?s=mall&c=api&m=favorite&delete=1&id="+id, function(data){
        //console.log(data);return;
        if (data == 1) {
            dr_tips("没有登录，不能收藏",3,1);
        } else if (data == 2) {
            dr_tips("商品不存在，无法收藏",3,1);
        } else if (data == 3) {
            //dr_tips("取消收藏成功", 3, 1);
            console.log('取消收藏成功');
        } else if (data == 4) {
            dr_tips("收藏成功", 3, 1);
        }
    });
}

// 购物车
var is_cart = true;
function select_dr_cart(id) {
    if (!is_cart) {
        //alert('刷新后再试');
        //return;
    }
    var cart = "{dr_url('order/cart/add', array('mid'=>MOD_DIR, 'cid'=>"+id+"))}";

    // 判断库存
    var _quantity = parseInt($("#commodityStockNum_"+id).val());
        //console.log(_quantity);return;
        if (isNaN(_quantity) || _quantity <= 0) {
            dr_tips("库存不足无法购买", 3);
            return;
        }
        //console.log(_quantity);return;
        //dr_sku_num
        var num = parseInt($("#commoditySelectNum_"+id).val());
        //console.log(num);return;
        if(num == 0){
            dr_tips("请选择购买数量", 3);
            return;
        }

        cart+= "&num="+num;
        is_cart = false;

    $.ajax({
        type: 'get',
        url: '/index.php?s=order&c=cart&m=add&mid=mall&cid='+id+'&num='+num,
        contentType: 'application/json;charset=utf-8',
        //data: "{'s': 'order', 'c': 'cart', 'm': 'add', 'mid': 'mall', 'cid': id, 'cid': id}",
        //data: '{"s": "order", "c": "cart", "m": "add", "mid": "mall", "cid": id, "num": num}',
        success: function (data) { //返回json结果
            //json转obj
            var obj = eval(data);
            //console.log(obj);return;
            if(obj.status == 1){
                alert('添加成功，购车中共'+obj.code+'件商品，请进入右上角购物车查看', 3, 1);
                $("#scart").html('购物车('+obj.code+')');
            }else{
                alert(obj.code, 3, 1);
            }
        }
    });

    }
    // 按规则更新商品价格
    function dr_sku_update_item() {
        var i = 0;
        var sku = new Array();
        // 遍历属性
        $(".dr_sku_value").each(function(){
            var id = $(this).val();
            if (id == "") return;
            sku[i] = id;
            i++;
        });
        var oname = sku.join("_"); // 组合串联属性
        // 当全部勾选之后才更新商品价格与库存
        var _sn = sku_sn[oname];
        var _price = sku_price[oname];
        var _quantity = sku_quantity[oname];
        $("#dr_sku_price").html(price_float(_price));
        $("#dr_sku_sn").html(_sn);
        if (_quantity) {
            $("#dr_sku_quantity").html(_quantity);
        } else {
            $("#dr_sku_quantity").html("<font color=red>库存不足</font>");
        }
    }
    // 按规则选择商品
    function dr_sku_select_item(_this, oname, val) {
        $(".dr_sku_"+oname+" > span").attr("class", "txt");
        $(_this).attr("class", "curr");
        $("#dr_sku_"+oname).val(val);
        dr_sku_update_item();
    }
    function price_float(value){  //保留两位小数点
        if (!value) value = 0;
        value = Math.round(parseFloat(value) * 100) / 100;
        if (value.toString().indexOf(".") < 0) {
            value = value.toString() + ".00";
        }
        return value;
    }
    // 设置商品的默认规则
    $(function(){
        $(".dr_sku_value").each(function(){
            var id = $(this).val();
            var oname = $(this).attr("oname");
            $(".dr_sku_"+oname+" > span").attr("class", "txt");
            if (id) {
                $("#dr_sku_value_"+id).attr("class", "curr");
            } else {
                var _first = $(".dr_sku_"+oname+" span").first();
                var _iid = _first.attr("ovalue");
                _first.attr("class", "curr");
                $("#dr_sku_"+oname).val(_iid);
            }
            dr_sku_update_item();
        });
    });


//监听input 复选框动作
/*
if($("input[name^='shoucang_select_']").is(':checked')) {
    // do something
    var class_name = $this.attr('class');
    console.log(class_name);
}


if($(".shoucang_select_28").is(':checked')) {
    // do something
    var class_name = $this.attr('class');
    console.log(class_name);
}

if($("input[type='checkbox']").is(':checked')){
    var class_name = $this.attr('class');
    console.log(class_name);
}

$(":checkbox").on("change",function(){
    //var $checkbox = $(this);
    //console.log($('input:checked').length);

    var class_name = $this.attr('class');
    console.log(class_name);
});

$("input[type='checkbox']").on("change",function(){
    var class_name = $this.attr('class');
    console.log(class_name);

});
*/

function check_checkbox_change(){
    $("input[type='checkbox']").on("change",function(){
        var class_name = $(this).prop('name');
        //console.log(class_name);return;

        if(class_name =='gouwuche_select'){
            //执行加入购物车
            //console.log(class_name);
            var tf = $("input[type='checkbox']").is(':checked');
            var id = $(this).val();
            //console.log(id);return;
            if(tf == true){
                //购物选中
                //console.log('购物选中');
                select_dr_cart(id);
            }else{
                //console.log('购物车未选中');
                select_dr_delete(id);
            }

        }else{
            //执行加入收藏
            var tf = $("input[type='checkbox']").is(':checked');
            var id = $(this).val();

            if(tf == true){
                //收藏成功
                dr_favorite(id);
            }else{
                //console.log('未选中');
                dr_favorite_del(id);
                //dr_tips("取消收藏成功", 3000, 1);
            }

        }
    });

}


//文字收藏特效
function shoucang_select_wenzi() {
    $(".shoucang_select_wenzi").click(function(){
        //var text = $(this).val();
        //console.log(text);
        //var t1=document.getElementsByClassName("test");
        //alert(t1.value);
        var id = $(this).attr("value");
        //console.log(id);
        var tf = $(this).html();
        //console.log(tf);
        if( tf == '收藏'){
            //收藏成功
            dr_favorite(id);
            $(this).html('已收藏'); //#da251d
            $(this).css("background-color","#da251d");
            $(this).css("color","#fff");
        }else{
            dr_favorite_del(id);
            //dr_tips("取消收藏成功", 3000, 1);
            $(this).html('收藏');
            $(this).css("background-color","#e8e8e8");
            $(this).css("color","#333");
        }
    });
}

//文字购物车特效
function gouwuche_select_wenzi(id) {
    $(".gouwuche_select_wenzi").click(function(){
        var id = $(this).attr("value");
        select_dr_cart(id);
        $("#commoditySelectNum_"+id).val(0);
    });
}


    function select_dr_delete(id) {
        //$('#dr_delete_load_'+id).show();
       /* $.ajax({type: "GET", url: "{dr_url('order/cart/delete')}&id="+id+"&"+Math.random(), dataType:"jsonp",
            success: function (data) {
            console.log(data);return;
                if (data.status) {
                    // 重新载入
                    //dr_load_cart();
                } else {
                    dr_tips(data.code);
                    //$('#dr_delete_load_'+id).hide();
                }
            }
        });*/

//console.log(id);return;
        $.ajax({
            type: 'get',
            url: '/index.php?s=order&c=cart&m=delete&mid=mall&id='+id+"&"+Math.random(),
            contentType: 'application/json;charset=utf-8',
            success: function (data) { //返回json结果
                var obj = eval(data);
                //console.log(obj);return;
                //json转obj
                var obj = eval(data);
                if(obj.status == 1){
                    alert('添加成功，购车中共'+obj.code+'件商品', 3, 1);
                    $("#scart").html('购物车('+obj.code+')');
                }else{
                    alert('未加入购物车', 3, 1);
                }
            }
        });

    }



